import os
import http.server
import socket
 
def start():
    handler.cgi_directories = ["index.html"]
    print("page available on :http://{}:{}".format(IP, PORT))
    httpd.serve_forever()

def clear():
    os.system('cls' if os.name == 'nt' else 'clear')

PORT = 8888
IP = (socket.gethostbyname(socket.gethostname()))
server_address = ("", PORT)
server = http.server.HTTPServer
handler = http.server.CGIHTTPRequestHandler
httpd = server(server_address, handler)
clear()
start()